package com.company;

import static java.lang.Math.*;

/**
 * @author Kristina Kosko
 */

/**Creating a trapezoid, sides are entering by a user,
 *calculating the square and the perimeter of a trapezoid
 */
public class Trapezoid {
    double a;
    double b;
    double c;
    double d;

    /**
     * brief information
     * Constructor of class Trapezoid
     * @param sides - 2 main sides and 2 sides of the trapezoid, array sides[] must contain 4 params
     *              1st - the smallest main side, the 2-nd - the biggest main side
                    the 3-rd - the smallest side, the 4-th - the biggest side
     */
    Trapezoid (double sides[]){
        a = sides[0];
        b = sides[1];
        c = sides[2];
        d = sides[3];
    }
    //Verifying the existence of trapezoid (each side mustn't be equal 0 or be < 0
    //The length of the biggest side must be less then the total length of the 3 others)
    public static boolean isFigureValid(double sides[]){
        for (int i = 0; i < sides.length; i++){
            if (sides[i] == 0 || sides[i] < 0
                    || sides[0]>sides[1]+sides[2]+sides[3]
                    || sides[1]>sides[2]+sides[3]+sides[0]
                    || sides[2]>sides[3]+sides[0]+sides[1]
                    ||sides[3]>sides[0]+sides[1]+sides[2]){
                System.out.println("Sorry, this trapezoid is not exist!");
                System.exit(1);
            }
        }
        return (true);
    }
    //Finds the perimeter of a trapezoid
    public double getPerimeter(){
       double perimeter = a+b+c+d;
        return(perimeter);
    }
    //Finds the square of a trapezoid, using the perimeter which was calculated in getPerimeter
    public double getSquare(){
        double p = this.getPerimeter();
        double square = (a+b)/abs(b-a)*sqrt((p-a)*(p-b)*(p-b-c)*(p-b-d));
        return(square);
    }
}
