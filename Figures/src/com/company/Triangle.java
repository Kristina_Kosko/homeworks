package com.company;


/**
 * @author Kristina Kosko
 */

/**Creating a triangle, sides are entering by a user,
 *calculating the square and the perimeter of a triangle
 */
public class Triangle {
    private double square;

    double a;
    double b;
    double c;

    /**
     * brief information
     * Constructor of class Triangle
     * @param sides - the sides of the triangle, array sides[] must contain 3 params
     */
    Triangle(double sides[]){
       a = sides[0];
       b = sides[1];
       c = sides[2];
    }
    //Verifying the existence of a triangle (each side mustn't be equal 0 or be < 0
    //and the length of the biggest side mustn't be bigger then the total length of the others)
    public static boolean isFigureValid(double sides[]){
        for (int i = 0; i < sides.length; i++){
            if (sides[i] == 0 ||sides[i] < 0
                    || sides[0]+sides[1]<sides[2]
                    || sides[1]+sides[2]<sides[0]
                    || sides[2]+sides[0]<sides[1]){
                System.out.println("Sorry, this triangle is not exist!");
                System.exit(1);
            }
        }
        return (true);
    }
    //Finds the perimeter of a triangle
    public double getPerimeter(){
         double perimeter = a + b + c;
         return (perimeter);
    }
    //Finds the square of a triangle, using the perimeter which was calculated in getPerimeter
    public double getSquare(){
        double perimeter = this.getPerimeter();
        square = Math.sqrt(perimeter*(perimeter-a)*(perimeter-b)*(perimeter-c));
        return (square);
    }
}

