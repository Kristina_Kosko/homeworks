package com.company;

/**
 * @author Kristina Kosko
 */

/**Creating an ellipse, radii are entering by a user,
 *calculating the square and the perimeter of an ellipse
 */
public class Ellipse {
    double a;
    double b;

    /**
     * brief information
     * Constructor of class Ellipse
     * @param sides - the radii of the ellipse, array sides[] must contain 2 params
     */
    Ellipse (double sides[]){
        a = sides[0];
        b = sides[1];
    }
    //Verifying the existence of an ellipse (each side mustn't be equal 0 or be < 0)
    public static boolean isFigureValid(double sides[]){
        for (int i = 0; i < sides.length; i++){
            if (sides[i] == 0 || sides[i] < 0){
                System.out.println("Sorry, this ellipse is not exist!");
                System.exit(1);
            }
        }
        return (true);
    }
    //Finds the perimeter of an ellipse
    public double getPerimeter(){
        double perimeter = 4*(3.14*2*a*b+(a-b))/(a+b);
        return (perimeter);
    }
    //Finds the perimeter of an ellipse
    public double getSquare(){
        double square = 3.14*a*b;
        return (square);
    }
}
