package com.company;

/**
 * @author Kristina Kosko
 */

/**Creating a rectangle, sides are entering by a user,
 *calculating the square and the perimeter of a rectangle
 */
public class Rectangle {
    double a;
    double b;

    /**
     * brief information
     * Constructor of class Rectangle
     * @param sides - the sides of the rectangle, array sides[] must contain 2 params
     */
    Rectangle (double sides[]){
        a = sides[0];
        b = sides[1];
    }
    //Verifying the existence of a rectangle (each side mustn't be equal 0 or be < 0)
    public static boolean isFigureValid(double sides[]){
        for (int i = 0; i < sides.length; i++){
            if (sides[i] == 0 || sides[i] < 0){
                System.out.println("Sorry, this rectangle is not exist!");
                System.exit(1);
            }
        }
        return (true);
    }
    //Finds the perimeter of a rectangle
    public double getPerimeter(){
        double perimeter = a + b;
    return (perimeter);
    }
    //Finds the perimeter of a rectangle
    public double getSquare(){
        double square = a*b/2;
        return (square);
    }
}
