package com.company;

/**
 @author Kristina Kosko
 @version 1.3
 */

import java.util.Scanner;

/**
 * Checks the type of figure, reads the sides of figure,
 * compute and print perimeter and square
 */
public class Main {

    /**
     * @param args - array of string type for the entered information from the console
     */
    public static void main(String[] args) {
        //Choosing the type of the figure
        Scanner check = new Scanner(System.in);
        System.out.println ("If your figure is triangle enter 1, if your figure is rectangle enter 2, ");
        System.out.println("if your figure is ellipse enter 3, if your figure is trapezoid enter 4:  ");
        int res;
        if (check.hasNextInt()){
            res = check.nextInt();
            if (res == 1){
                Scanner in = new Scanner(System.in);
                double[] sides = new double[3];
                System.out.println("Enter the length of the sides of the triangle: ");
                if (in.hasNextDouble()) {
                    for (int i = 0; i < sides.length; i++) {
                        sides[i] = in.nextDouble();
                    }
                }
                if (Triangle.isFigureValid(sides)) {
                    Triangle triangle = new Triangle(sides);
                    System.out.println("Perimeter of the triangle is: " + triangle.getPerimeter());
                    System.out.println("Square of the triangle is: " + triangle.getSquare());
                }
            }else if (res == 2){
                     Scanner in = new Scanner (System.in);
                     double[] sides = new double[2];
                     System.out.println("Enter the length of the sides of your rectangle: ");
                     if (in.hasNextDouble()) {
                         for (int i = 0; i < sides.length; i++) {
                             sides[i] = in.nextDouble();
                         }
                     }
                         if(Rectangle.isFigureValid(sides)) {
                            Rectangle rectangle = new Rectangle(sides);
                            System.out.println("Perimeter of the rectangle is: " + rectangle.getPerimeter());
                            System.out.println("Square of the rectangle is: " + rectangle.getSquare());
                         }
                     }else if (res == 3){
                             Scanner in = new Scanner (System.in);
                             double[] sides = new double[2];
                             System.out.println("Enter the radii of your ellipse: ");
                                 if (in.hasNextDouble()) {
                                     for (int i = 0; i < sides.length; i++) {
                                         sides[i] = in.nextDouble();
                                     }
                                 }
                                 if (Ellipse.isFigureValid(sides)) {

                                     Ellipse ellipse = new Ellipse(sides);
                                     System.out.println("Perimeter of the ellipse is: " + ellipse.getPerimeter());
                                     System.out.println("Square of the ellipse is: " + ellipse.getSquare());
                                 }
                     }else if(res == 4){
                              Scanner in = new Scanner(System.in);
                              double[] sides = new double[4];
                              System.out.println("Enter the length of the sides of you trapezoid");
                              System.out.println("(put at 1-st - the smallest main side, the 2-nd - the biggest main side;");
                              System.out.println("the 3-rd - the smallest side, the 4-th - the biggest side): ");
                              if (in.hasNextDouble()){
                                  for (int i=0; i<sides.length;i++ ) {
                                       sides[i] = in.nextDouble();
                                  }
                              }
                              if(Trapezoid.isFigureValid(sides)) {
                                  Trapezoid trapezoid = new Trapezoid(sides);
                                  System.out.println("Perimeter of the trapezoid is: " + trapezoid.getPerimeter());
                                  System.out.println("Square of the trapezoid is: " + trapezoid.getSquare());
                              }
                     }
            //If the user entered the wrong identifier of the figure's type the program is finishing
            else {
                   System.out.println("You used Wrong identifier!");
                   System.exit(1);
            }
        }




    }
}
